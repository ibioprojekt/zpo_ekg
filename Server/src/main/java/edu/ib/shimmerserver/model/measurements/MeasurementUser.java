package edu.ib.shimmerserver.model.measurements;

import java.util.ArrayList;

public class MeasurementUser {
    private String userName;
    private ArrayList<MeasurementSeries> measurementSeries;

    public MeasurementUser(String userName, ArrayList<MeasurementSeries> measurementSeries) {
        this.userName = userName;
        this.measurementSeries = measurementSeries;
    }

    public MeasurementUser() {
        this.measurementSeries = new ArrayList<>();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ArrayList<MeasurementSeries> getMeasurementSeries() {
        return measurementSeries;
    }

    public void setMeasurementSeries(ArrayList<MeasurementSeries> measurementSeries) {
        this.measurementSeries = measurementSeries;
    }

    @Override
    public String toString() {
        return "MeasurementUser{" +
                "userName='" + userName + '\'' +
                ", measurementSeries=" + measurementSeries +
                '}';
    }
}
