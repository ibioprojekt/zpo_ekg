package edu.ib.shimmerserver.model.measurements;

import java.util.ArrayList;

public class MeasurementSeries {

    private String type;
    private String date;
    private ArrayList<Double> measurementPoints;

    public MeasurementSeries(String type, String date, ArrayList<Double> measurementPoints) {
        this.type = type;
        this.date = date;
        this.measurementPoints = measurementPoints;
    }

    public MeasurementSeries() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public ArrayList<Double> getMeasurementPoints() {
        return measurementPoints;
    }

    public void setMeasurementPoints(ArrayList<Double> measurementPoints) {
        this.measurementPoints = measurementPoints;
    }

    @Override
    public String toString() {
        return "MeasurementSeries{" +
                "type='" + type + '\'' +
                ", date='" + date + '\'' +
                ", measurementPointsLen=" + measurementPoints.size() +
                '}';
    }
}
