package edu.ib.shimmerserver.firebase;

import com.google.firebase.database.*;
import edu.ib.shimmerserver.model.measurements.MeasurementSeries;
import edu.ib.shimmerserver.model.measurements.MeasurementUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class FirebaseDatabaseHelper {

    private FirebaseDatabase mDatabase;
    private DatabaseReference dbRef;
//    private MeasurementSeries measurementSeries = new MeasurementSeries();



    public void getUserMeasurements(String username){
        mDatabase = FirebaseDatabase.getInstance();
        dbRef = mDatabase.getReference().child(username);


        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                MeasurementUser userMeasurements = new MeasurementUser();

                userMeasurements.setUserName(username);

                for (Iterator<DataSnapshot> it = dataSnapshot.getChildren().iterator(); it.hasNext(); ) {
                    DataSnapshot child = it.next();
                    String date = child.getKey();
                    for (Iterator<DataSnapshot> iter = child.getChildren().iterator(); iter.hasNext(); ) {

                        MeasurementSeries measurementSeries = new MeasurementSeries();
                        measurementSeries.setDate(date);
                        DataSnapshot child2 = iter.next();
                        measurementSeries.setType(child2.getKey());

                        ArrayList<HashMap> value = (ArrayList<HashMap>) child2.getValue();
                        ArrayList measurementPoints = new ArrayList<>();
                        for (HashMap hm: value
                             ) {
                            measurementPoints.add(hm.get("value"));
                        }

//                        measurementUser.getMeasurementSeries().add(measurementSeries);

                        measurementSeries.setMeasurementPoints(measurementPoints);

                        userMeasurements.getMeasurementSeries().add(measurementSeries);

                    }
                }

//                setMeasurementSeries(userMeasurements.getMeasurementSeries().get(0));
                System.out.println(userMeasurements.getMeasurementSeries().get(0).getMeasurementPoints());



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // added for clarity
                System.out.println("The read failed - code: " + databaseError.getCode());
            }
        });

        System.out.println("checkpoint");

//        return measurementUser;

    }
//
//    public void setMeasurementSeries(MeasurementSeries measurementSeries) {
//        this.measurementSeries = measurementSeries;
//    }
//
//    public MeasurementSeries getMeasurementSeries() {
//        return measurementSeries;
//    }
//
//        @Bean
//    public MeasurementSeries measurementSeries(){
//        return this.measurementSeries;
//    }
}
