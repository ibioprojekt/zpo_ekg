package edu.ib.shimmerserver.restcontrollers;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserRecord;
import com.google.gson.Gson;
import edu.ib.shimmerserver.firebase.FirebaseDatabaseHelper;
import edu.ib.shimmerserver.model.user.UserCredentials;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {
    @GetMapping(value = "/login")
    public String getLoginPage(){
        return "login";
    }

    @PostMapping(value = "/token")
    public ResponseEntity getToken(@RequestBody UserCredentials userCredentials, HttpServletRequest request, HttpServletResponse response){

        Gson gson = new Gson();

        if(userCredentials.getEmail().isEmpty() || userCredentials.getPassword().isEmpty()) {

            Map<String,String> message = new HashMap<>();
            message.put("message","You need to insert email and password");

            String result = gson.toJson(message);

            return new ResponseEntity(result,HttpStatus.BAD_REQUEST);
        }

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        try {
            UserRecord userByEmail = firebaseAuth.getUserByEmail(userCredentials.getEmail());

            String customToken = FirebaseAuth.getInstance().createCustomToken(userByEmail.getUid());

            // set Cookies
            Cookie c1 = new Cookie("current_user_email", String.valueOf(userCredentials.getEmail()));
            c1.setMaxAge(3600);
            response.addCookie(c1);

            int index = String.valueOf(userCredentials.getEmail()).indexOf('@');
            String userTag = String.valueOf(userCredentials.getEmail()).substring(0,index);

            Cookie c2 = new Cookie("current_user_tag", userTag);
            c2.setMaxAge(3600);
            response.addCookie(c2);
            // set Cookies

            Authentication auth =
                    new UsernamePasswordAuthenticationToken(userCredentials.getEmail(), null, userCredentials.getAuthorities());
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);

            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);


            System.out.println(SecurityContextHolder.getContext().getAuthentication().isAuthenticated());

            return new ResponseEntity(customToken,HttpStatus.OK);

        } catch (FirebaseAuthException e) {
            Map<String,String> message = new HashMap<>();
            message.put("message","Authentication failed");

            String result = gson.toJson(message);
            return new ResponseEntity(result,HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/principal")
    public Principal user() {

        return (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
