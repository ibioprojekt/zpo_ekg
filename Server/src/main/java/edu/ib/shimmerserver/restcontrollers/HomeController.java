package edu.ib.shimmerserver.restcontrollers;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import edu.ib.shimmerserver.firebase.FirebaseDatabaseHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    private FirebaseDatabase mDatabase;
    private FirebaseDatabaseHelper firebaseDatabaseHelper;
    private DatabaseReference dbRef;

    @GetMapping("/")
    public String getHomePage() {
        return "index";
    }

    @GetMapping("/profile")
    public String getProfile(){
        return "profile";
    }


//    @GetMapping("/api/data/getData")
//    public List<Double> measurements(@RequestParam(name = "username") String username) {
//
//        return null;
//    }

}

