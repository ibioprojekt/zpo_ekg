package edu.ib.shimmerserver;

import edu.ib.shimmerserver.firebase.FirebaseDatabaseHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class ShimmerserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShimmerserverApplication.class, args);
    }

//    Only for testing
    @EventListener(ApplicationReadyEvent.class)
    public void testFirebaseConnection(){
        FirebaseDatabaseHelper firebaseDatabaseHelper = new FirebaseDatabaseHelper();
        firebaseDatabaseHelper.getUserMeasurements("TestUser");

        //
    }
}
