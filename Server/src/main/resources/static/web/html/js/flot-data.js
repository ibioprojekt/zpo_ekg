//Flot Line Chart
$(document).ready(function () {
    console.log("document ready");
    //
    // var y = [];
    // var x = [];
    // const dataRequest = new XMLHttpRequest();
    // const url = "api/data/getData?username=TestUser";
    // dataRequest.open("GET", url);
    // dataRequest.send();
    //
    // dataRequest.onreadystatechange = ((ev) => {
    //         console.log("something received")
    //         console.log(dataRequest.responseType);
    //         let retrievedData = dataRequest.response;
    //         console.log(retrievedData)
    //         for(let i = 0; retrievedData.size(); i++) {
    //             y.push(retrievedData[i]);
    //             x.push(i);
    //         }
    //
    // });


    var offset = 0;
    // plot();

    function plot(y) {
        // var y = [],
            cos = [];
        // for (let i = 0; i < 100; i ++) {
        //     y.push([i, Math.random()]);
        //     // cos.push([i, Math.cos(i + offset)]);
        // }
        var options = {
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true
                }
            },
            grid: {
                hoverable: true //IMPORTANT! this is needed for tooltip to work
            },
            yaxis: {
                min: -1.2,
                max: 1.2
            },
            colors: ["#009efb", "#55ce63"],
            grid: {
                color: "#AFAFAF",
                hoverable: true,
                borderWidth: 0,
                backgroundColor: '#FFF'
            },
            tooltip: true,
            tooltipOpts: {
                content: "'%s' of %x.1 is %y.4",
                shifts: {
                    x: -60,
                    y: 25
                }
            }
        };
        var plotObj = $.plot($("#flot-line-chart"), [{
            data: y,
            label: "random number(x)",
        }, {
            data: [],
            label: ""
        }], options);
    }
});
