function getCookieValue(cookieUserTag) {
    var value = document.cookie.match('(^|[^;]+)\\s*' + cookieUserTag + '\\s*=\\s*([^;]+)');
    console.log("Got cookie: " + value);
    return value;
}


function reloadCss() {
    var links = document.getElementsByTagName("link");
    for (var cl in links) {
        var link = links[cl];
        if (link.rel === "stylesheet")
            link.href += "";
    }
}

function setUserWelcomePrompt() {
    let user = getCookieValue("current_user_tag")[2];
    document.getElementById("welcome-prompt").innerText = "Welcome, " + user;
}


var firebaseApp = null;
var firebaseDB = null;
var childData = null;


function firebaseConnect() {

    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyD5eRD6ySLy04UylDbKnVTgcdv8n7Atnew",
        authDomain: "shimmerzpo.firebaseapp.com",
        databaseURL: "https://shimmerzpo.firebaseio.com",
        projectId: "shimmerzpo",
        storageBucket: "shimmerzpo.appspot.com",
        messagingSenderId: "362340323337",
        appId: "1:362340323337:web:15e78ebecbd521a35268d8"
    };
    // Initialize Firebase

    firebaseApp = firebase.initializeApp(firebaseConfig);
    console.log(firebaseApp);
}

function databaseConnect() {
    firebaseDB = firebase.database();
    getData();
}

function getData() {

    // Setting current month on month selector
    document.getElementById("month-selector").value = '13';

    // Loop through users in order with the forEach() method. The callback
    // provided to forEach() will be called synchronously with a DataSnapshot
    // for each child:
    // firebaseDB = firebase.database();
    console.log("firebaseApp: " + firebaseApp + "firebaseDB: " + firebaseDB);

    if (firebaseApp !== null && firebaseDB !== null) {

        var rootRef = firebaseDB.ref();
        var childRef = rootRef.child("/" + cookieValue[2]);
        childRef.once("value")
            .then(function (snapshot) {
                childData = JSON.parse(JSON.stringify(snapshot.val()));
                console.log(JSON.parse(JSON.stringify(childData)));
                // snapshot.forEach(function(child) {
                //     console.log(child.key+": "+child.val());
                //     childData = child.val();
                // });

                // Raczej musi byc w tym miejscu - bo korzysta z childData.
                populateTable();
            });

    }

}


function populateTable() {

    console.log("POPULATE TABLE");

    let table = document.getElementById("measurements-table");
    let tbody = table.lastElementChild;
    let samplesSum = 0;
    let totalMeasurements = 0;


    console.log(childData);

    if (childData !== null) {


        // Get keys for user's data json. These are the dates of measurements.
        let jsonKeys = Object.keys(childData);
        console.log('Keys1: ', jsonKeys)


        let selectedMonth = document.getElementById("month-selector").value;

        if (selectedMonth < 10) {
            selectedMonth = "0" + selectedMonth;
        }

        console.log(selectedMonth);


        // jsonKeys.filter(isEqual);

        if (selectedMonth != 13) {
            jsonKeys = jsonKeys.filter(value => value.substr(5, 2) == selectedMonth);
        }

        $("#measurements-table tr").remove();

        jsonKeys.forEach(function (value, index) {
            console.log(index + ' ' + value)
            // Get measurement type for given date
            let measurementsTypes = Object.keys(childData[value]);
            totalMeasurements = measurementsTypes.length;
            console.log('measurement type: ' + measurementsTypes)
            // For every measurement type populate table with it's information.
            // At the end there is also appended button for plotting given measurements points
            measurementsTypes.forEach(function (value2, index2) {


                let row = tbody.insertRow(-1);
                let dateCell = row.insertCell(0);
                dateCell.innerText = value;

                let measurementCell = row.insertCell(1);
                measurementCell.innerText = value2;

                let measurementPointsCell = row.insertCell(2);
                let measurementSize = childData[value][value2].length;
                measurementPointsCell.innerHTML = measurementSize;

                samplesSum += measurementSize;

                let array = [];

                array = childData[value][value2];

                let plotCell = row.insertCell(3);

                let plotBtn = document.createElement("input");
                plotBtn.type = "button";
                plotBtn.className = "btn btn-danger";
                plotBtn.value = "Plot";
                plotBtn.onclick = (function () {
                    plotMeasurements(array, value, value2);

                });

                plotCell.appendChild(plotBtn);

            })

        })
    }

    // Setting statistics for current user with total amount of measurments and samples
    document.getElementById("total-samples").innerText = samplesSum;
    document.getElementById("total-measurements").innerText = totalMeasurements;


    reloadCss();
}


// THIS IS PLACEHOLDER FOR PLOTTING LOGIC
function plotMeasurements(measurements, date, type) {
    console.log("PLOTTING : " + measurements)

    let offset = 0;
    let y = [];

    for (let i = 0; i < measurements.length; i++) {
        y.push(measurements[i]['value']);
    }

    console.log(typeof y[0]);
    console.log(y);
    plot(y, date, type);


}



// var offset = 0;
// plot();

function plot(array, date, type) {
    // var y = [],
    // for (let i = 0; i < 100; i ++) {
    //     y.push([i, Math.random()]);
    //     // cos.push([i, Math.cos(i + offset)]);
    // }

    let charContent = document.getElementById("flot-line-chart");
    let offsetContent = document.getElementById("flot-line-offset");

    console.log(typeof array)

    let step_size = 1;

    let pointsData = []

    let maxVal = array[0]
    let minVal = array[0]

    for (let i = 0; i < array.length; i++) {
        pointsData.push([i * step_size, array[i]]);
        if (maxVal < array[i])
            maxVal = array[i]
        if (minVal > array[i])
            minVal = array[i]
    }

    var options = {
        series: {
            lines: {
                show: true
            },
            points: {
                show: false
            }
        },
        grid: {
            hoverable: true, //IMPORTANT! this is needed for tooltip to work
        },
        yaxis: {
            min: minVal  -1,
            max: maxVal + 1
        },

        colors: ["#009efb", "#55ce63"],
        grid: {
            color: "#AFAFAF",
            hoverable: true,
            borderWidth: 0,
            backgroundColor: '#FFF'
        },
        tooltip: true,
        tooltipOpts: {
            content: "'%s' of %x.1 is %y.4",
            shifts: {
                x: -60,
                y: 25
            }
        },

    };

    var zoomOptions = {
        series: {
            lines: {
                show: true
            },
            points: {
                show: false
            }
        },
        selection : {
            mode:"x"
        },
        grid: {
            hoverable: true, //IMPORTANT! this is needed for tooltip to work
        },
        yaxis: {
            min: minVal  -1,
            max: maxVal + 1
        },

        colors: ["#009efb", "#55ce63"],
        grid: {
            color: "#AFAFAF",
            hoverable: true,
            borderWidth: 0,
            backgroundColor: '#FFF'
        },
        tooltip: true,
        tooltipOpts: {
            content: "'%s' of %x.1 is %y.4",
            shifts: {
                x: -60,
                y: 25
            }
        },

    };

    console.log('MAX Y : ',maxVal);
    console.log('MIN Y : ', minVal);

    var plotObj = $.plot(charContent, [{
            data: pointsData,
            label: "ECG from " + date + ", type: " + type,
        }]
        , options);

    var zoomObj = $.plot(offsetContent, [{
        data: pointsData,
        label: "ECG from " + date + ", type: " + type,

    }],zoomOptions)

    $("#flot-line-offset").bind("plotselected",function (event,ranges) {
        plotObj.setSelection(ranges);
    });

    $("#flot-line-chart").bind("plotselected",function (event,ranges) {
        if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {ranges.xaxis.to = ranges.xaxis.from + 0.00001;}
        if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {ranges.yaxis.to = ranges.yaxis.from + 0.00001;}

        var zoomoptions = { series: {
                lines: {
                    show: true
                },
                points: {
                    show: false
                }
            }}

        console.log("X RANGE:",ranges.xaxis.from," : ",ranges.xaxis.to)

        let zoomedData = array.slice(parseInt(ranges.xaxis.from),parseInt(ranges.xaxis.to));
        let zoomedPlotData = []


        let zmaxVal = zoomedData[0]
        let zminVal = zoomedData[0]

        for (let i = 0; i < zoomedData.length; i++) {
            zoomedPlotData.push([i * step_size, zoomedData[i]]);
            if (zmaxVal < zoomedData[i])
                zmaxVal = zoomedData[i]
            if (zminVal > zoomedData[i])
                zminVal = zoomedData[i]
        }
        console.log('ZOOMED: ',zoomedData);

        let plotZoomed = $.plot(charContent,[{
                data: zoomedPlotData,
                label: "ECG from " + date + ", type: " + type,
            }],
            $.extend(true,{},zoomoptions,{
                yaxis: {
                    min: zminVal  -1,
                    max: zmaxVal + 1
                }
            }));
        zoomObj.setSelection(ranges,true);
    });

}


function onSelectedChange() {
    populateTable();
}



