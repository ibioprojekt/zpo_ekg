function reloadCss() {
    var links = document.getElementsByTagName("link");
    for (var cl in links) {
        var link = links[cl];
        if (link.rel === "stylesheet")
            link.href += "";
    }
}


function authorize(){


    let emailInput = document.getElementById("inputEmail");
    let passwordInput = document.getElementById("inputPassword");
    let errorLabel = document.getElementById("loginError");


    console.log(emailInput.value);
    console.log(passwordInput.value);

    $.ajax("/token",{
        type:'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            email:emailInput.value,
            password:passwordInput.value
        }),
        success:function (token) {
            
            firebase.auth().signInWithCustomToken(token).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
            }).finally(function () {

                $.ajax("/",{
                    type:'GET',
                    success: function () {
                        $(location).attr('href','/');
                    }
                });

            });


        },
        error:function (result) {

            console.log(result)

            console.log(JSON.parse(result.responseText).message);

            let messageNode = document.createTextNode(JSON.parse(result.responseText).message);
            errorLabel.appendChild(messageNode);
            reloadCss();

            setTimeout(function () {
                errorLabel.removeChild(messageNode);
            }, 4000);
        }
    });
}