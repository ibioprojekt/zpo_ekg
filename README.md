# ZPO EKG SHIMMER

## Michał Marciniak
## Stanisław Zamora
## Arkadiusz Kowal

Projekt stworzony na potrzeby zajęć z zaawansowanego programowania obiektowego. Celem było stworzenie systemu umożliwiającego akwizycję i podgląd danych zebranych w procesie pomiaru EKG.
Wykorzystano czujnik Shimmer. Projekt składa się z: aplikacja na telefon, strona webowa i serwer.
>
## Wykorzystano technologie:
* SpringBoot (Web,Data,Security)
* Firebase (RealtimeDatabase,Authentication)
* Gson
* Maven
* jQuery
* flot.js
* Android

>
Aplikacja implementuje architekturę klient-serwer i jest napisana zgodnie z wzorcem MVC. 
>
## Kod serwera i aplikacji webowej:
[WEB](https://gitlab.com/ibioprojekt/zpo_ekg/-/tree/master/Server)
>
## Kod aplikacji na androida (rozwijany na bazie kodu twórców Shimmera):
[Android](https://gitlab.com/ibioprojekt/zpo_ekg/-/tree/master/shimmerAPI/ShimmerAndroidInstrumentDriver/shimmerServiceExample/src/main/java/com/shimmerresearch/shimmerserviceexample)

## Dokumentacja:
[dokumentacja](https://gitlab.com/ibioprojekt/zpo_ekg/-/blob/master/docs/dokumentacja.pdf)