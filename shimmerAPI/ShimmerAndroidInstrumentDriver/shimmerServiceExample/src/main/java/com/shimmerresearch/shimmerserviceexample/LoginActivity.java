package com.shimmerresearch.shimmerserviceexample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity{

    public static final String TAG = "IBActivity";
    private FirebaseAuth mAuth;

    public Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_panel);

        mAuth = FirebaseAuth.getInstance();
        btnSignIn = (Button) findViewById(R.id.SignBtn);
    }

    public void onSignIn(View view) {
        EditText edtEmail = (EditText) findViewById(R.id.emailTV);
        EditText edtPassword = (EditText) findViewById(R.id.passwordTV);

        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this,(task)->{
                    if(task.isSuccessful()){
                        Log.d(TAG,"signWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        Toast.makeText(getApplicationContext(),"Authentication OK.",Toast.LENGTH_LONG).show();

                        Intent myIntent = new Intent(LoginActivity.this,MainActivity.class);
                        String userName = user.getEmail().split("@")[0].replace(".","");
                        myIntent.putExtra("FIREBASE_USER",userName);
                        startActivity(myIntent);
                    }
                    else {
                        Log.d(TAG,"signInWithEmail:failure",task.getException());
                        Toast.makeText(getApplicationContext(),"Authentication failed.",Toast.LENGTH_LONG).show();
                    }
                });

    }

    public void onSignUp(View view) {

        EditText edtEmail = (EditText) findViewById(R.id.emailTV);
        EditText edtPassword = (EditText) findViewById(R.id.passwordTV);

        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();


        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG,"createUserWithEmail:success");
                    Toast.makeText(getApplicationContext(),"Account created.",Toast.LENGTH_LONG).show();
                    FirebaseUser user = mAuth.getCurrentUser();
                }
                else {
                    Log.w(TAG,"createUserWithEmail:failure", task.getException());
                    Toast.makeText(getApplicationContext(),"Account was not created",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
