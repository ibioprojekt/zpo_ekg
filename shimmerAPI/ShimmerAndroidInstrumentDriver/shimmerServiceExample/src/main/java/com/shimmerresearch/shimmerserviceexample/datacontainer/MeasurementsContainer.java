package com.shimmerresearch.shimmerserviceexample.datacontainer;

import com.shimmerresearch.driver.Configuration;

import java.util.ArrayList;

public class MeasurementsContainer {


    private static ArrayList<MeasurementPoint> ecg_LA_RA = new ArrayList<>();
    private static ArrayList<MeasurementPoint> ecg_LA_RL = new ArrayList<>();
    private static ArrayList<MeasurementPoint> ecg_LL_RA = new ArrayList<>();
    private static ArrayList<MeasurementPoint> ecg_LL_LA = new ArrayList<>();
    private static ArrayList<MeasurementPoint> timestamps = new ArrayList<>();

    private static ArrayList<String> formats = new ArrayList<String>() {
        {
            add(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LA_RA_24BIT);
            add(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LA_RL_24BIT);
            add(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LL_RA_24BIT);
            add(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LL_LA_24BIT);
            add(Configuration.Shimmer3.ObjectClusterSensorName.TIMESTAMP);
        }
    };

    public static void clearContainers(){
        ecg_LA_RA.clear();
        ecg_LA_RL.clear();
        ecg_LL_RA.clear();
        ecg_LL_LA.clear();
        timestamps.clear();
    }

    public static ArrayList<MeasurementPoint> getEcg_LA_RA() {
        return ecg_LA_RA;
    }

    public static ArrayList<MeasurementPoint> getEcg_LA_RL() {
        return ecg_LA_RL;
    }

    public static ArrayList<MeasurementPoint> getEcg_LL_RA() {
        return ecg_LL_RA;
    }

    public static ArrayList<MeasurementPoint> getEcg_LL_LA() {
        return ecg_LL_LA;
    }

    public static ArrayList<MeasurementPoint> getTimestamps() {
        return timestamps;
    }

    public static ArrayList<String> getFormats() {
        return formats;
    }
}
