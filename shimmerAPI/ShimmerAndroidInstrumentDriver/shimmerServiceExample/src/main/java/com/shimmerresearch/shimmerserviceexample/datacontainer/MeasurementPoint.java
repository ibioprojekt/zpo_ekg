package com.shimmerresearch.shimmerserviceexample.datacontainer;

public class MeasurementPoint {
    Double value;

    public MeasurementPoint() {
    }

    public MeasurementPoint(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}