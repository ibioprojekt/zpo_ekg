package com.shimmerresearch.shimmerserviceexample;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.shimmerresearch.driver.Configuration;
import com.shimmerresearch.shimmerserviceexample.datacontainer.MeasurementsContainer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FirebaseDataDialog extends AppCompatDialogFragment {



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Saving data dialog")
                .setMessage("Do you want to save data in firebase?")
                .setPositiveButton("save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child(MainActivity.userEmail);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.getDefault());
                        String currentDateTime = sdf.format(Calendar.getInstance().getTime());

                        for (String format : MeasurementsContainer.getFormats()) {
                            if (format.equals(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LA_RA_24BIT)) {
                                if (MeasurementsContainer.getEcg_LL_RA().size() > 1) {
                                    dbRef.child(currentDateTime).child("LA_RA").setValue(MeasurementsContainer.getEcg_LA_RA());
                                }
                            } else if (format.equals(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LA_RL_24BIT)) {
                                if (MeasurementsContainer.getEcg_LA_RL().size() > 1) {
                                    dbRef.child(currentDateTime).child("LA_RL").setValue(MeasurementsContainer.getEcg_LA_RL());
                                }
                            } else if (format.equals(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LL_RA_24BIT)) {
                                if (MeasurementsContainer.getEcg_LL_RA().size() > 1) {
                                    dbRef.child(currentDateTime).child("LL_RA").setValue(MeasurementsContainer.getEcg_LL_RA());
                                }
                            } else if (format.equals(Configuration.Shimmer3.ObjectClusterSensorName.ECG_LL_LA_24BIT)) {
                                if (MeasurementsContainer.getEcg_LL_LA().size() > 1) {
                                    dbRef.child(currentDateTime).child("LL_LA").setValue(MeasurementsContainer.getEcg_LL_LA());
                                }
                            }
                        }
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getContext(),"Saving abandoned",Toast.LENGTH_SHORT).show();



                    }
                });
        return builder.create();
    }
}
